/********************************************************************************
** Form generated from reading UI file 'qt_cv2.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_CV2_H
#define UI_QT_CV2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_qt_cv2Class
{
public:
    QAction *actionVolba1;
    QWidget *centralWidget;
    QLabel *label;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *qt_cv2Class)
    {
        if (qt_cv2Class->objectName().isEmpty())
            qt_cv2Class->setObjectName(QStringLiteral("qt_cv2Class"));
        qt_cv2Class->resize(619, 420);
        actionVolba1 = new QAction(qt_cv2Class);
        actionVolba1->setObjectName(QStringLiteral("actionVolba1"));
        centralWidget = new QWidget(qt_cv2Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 161, 41));
        qt_cv2Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(qt_cv2Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 619, 21));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        qt_cv2Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(qt_cv2Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        qt_cv2Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(qt_cv2Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        qt_cv2Class->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionVolba1);

        retranslateUi(qt_cv2Class);
        QObject::connect(actionVolba1, SIGNAL(triggered()), qt_cv2Class, SLOT(akcia1_click()));

        QMetaObject::connectSlotsByName(qt_cv2Class);
    } // setupUi

    void retranslateUi(QMainWindow *qt_cv2Class)
    {
        qt_cv2Class->setWindowTitle(QApplication::translate("qt_cv2Class", "qt_cv2", 0));
        actionVolba1->setText(QApplication::translate("qt_cv2Class", "Volba1", 0));
        label->setText(QApplication::translate("qt_cv2Class", "TextLabel", 0));
        menuMenu->setTitle(QApplication::translate("qt_cv2Class", "Menu", 0));
    } // retranslateUi

};

namespace Ui {
    class qt_cv2Class: public Ui_qt_cv2Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_CV2_H
