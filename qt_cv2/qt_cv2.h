#ifndef QT_CV2_H
#define QT_CV2_H

#include <QtWidgets/QMainWindow>
#include "ui_qt_cv2.h"
#include <qmessagebox.h>
#include <vector>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>

class qt_cv2 : public QMainWindow
{
	Q_OBJECT

public:
	qt_cv2(QWidget *parent = 0);
	~qt_cv2();

public slots:
	void akcia1_click();

private:
	int cislo=10;
	QString retazec;
	std::vector<double> poled;
	Ui::qt_cv2Class ui;
};

#endif // QT_CV2_H
